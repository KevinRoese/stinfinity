<?php
header("Content-Type: application/json; charset=utf-8"); 

/*
 * states of avatars:
 * 0 - in use
 * 1 - blocked
 * 2 - special
 * */

/*
 * avaadd(id, state, avatar, use, notes)->succ
 * avaedit(id, state, avatar, use, notes)->succ
 * avadel(id)->succ
 * list->data
 * */

//login data omitted
$db_server = 'xxxxxxxx';
$db2_user = 'xxxxxxxx';
$db2_password= 'xxxxxxxx';
$db2_name = 'xxxxxxxx';

$con2 = mysqli_connect($db_server, $db2_user, $db2_password, $db2_name);

$str_in = file_get_contents("php://input");
$in = json_decode($str_in);

$data = array();

switch($in->type)
{
	case "avaadd":
		$sql = "INSERT INTO forum_avatars (`id`, `state`, `avatar`, `used_for`, `notes`) VALUES ('" . $in->id . "', '" . $in->state . "', '" . $in->avatar . "', '" . $in->use . "', '" . $in->notes . "')";
		if ($con2->query($sql) === TRUE) {
			ack();
		} else {
			echo "Error: " . $sql . "<br>" . $con2->error;
		}
		break;
		
	case "avaedit":
		$sql = "UPDATE forum_avatars SET state = '" . $in->state . "', avatar = '" . $in->avatar . "', used_for = '" . $in->use . "', notes = '" . $in->notes . "' WHERE id = " . $in->id;
		if ($con2->query($sql) === TRUE) {
			ack();
		} else {
			echo "Error: " . $sql . "<br>" . $con2->error;
		}
		break;
		
	case "avadel":
		$sql = "DELETE FROM forum_avatars WHERE id = " . $in->id;
		if ($con2->query($sql) === TRUE) {
			ack();
		} else {
			echo "Error: " . $sql . "<br>" . $con2->error;
		}
		break;
		
	case "list":
		if ($result = mysqli_query($con2, "SELECT * FROM forum_avatars")) {
			while($row = $result->fetch_array(MYSQLI_ASSOC)) {
				$custom = array();
				$custom["id"] = intval($row["id"]);
				$custom["state"] = intval($row["state"]);
				$custom["avatar"] = utf8_encode($row["avatar"]);
				$custom["use"] = utf8_encode($row["used_for"]);
				$custom["notes"] = utf8_encode($row["notes"]);
				$data["avatars"][intval($row["id"])] = $custom;
			}
			// Free result set
			mysqli_free_result($result);
		}
		$arr = array("type"=>"list", "data"=>$data);
		$str_json = json_encode($arr);
		echo $str_json;
		break;
}

function ack()
{
	$arr = array("type"=>"succ");
	$str_json = json_encode($arr);
	echo $str_json;
}


mysqli_close($con2);
?>
