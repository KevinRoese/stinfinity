<?php
header("Content-Type: application/json; charset=utf-8"); 

/*
 * offadd(id, name, rank, post)->succ
 * offedit(id, name, rank, post)->succ
 * shipadd(id, name, image, link, number)->succ
 * shipedit(id, name, image, link, number)->succ
 * offmove(id, dest[holiday, newship], hadship, oldship)->succ
 * list->data
 * */

$db_server = 'xxxxxxxx';
$db2_user = 'xxxxxxxxxx';
$db2_password= 'xxxxxxxxxxxxxxx';
$db2_name = 'xxxxxxxxxxxx';

$con2 = mysqli_connect($db_server, $db2_user, $db2_password, $db2_name);

$str_in = file_get_contents("php://input");
$in = json_decode($str_in);

$data = array();

switch($in->type)
{
	case "offadd":
		$sql = "INSERT INTO forum_char (`id`, `link_id`, `forum_id`, `player_id`, `rank`, `post`, `type`, `name`, `holiday`) VALUES ('" . $in->id . "', '-1', NULL, NULL, '" . $in->rank . "', '" . $in->post . "', '" . $in->chartype . "', '" . $in->name . "', '" . $in->holiday . "')";
		if ($con2->query($sql) === TRUE) {
			ack();
		} else {
			echo "Error: " . $sql . "<br>" . $con2->error;
		}
		break;
		
	case "offedit":
		$sql = "UPDATE forum_char SET name = '" . $in->name . "', rank = '" . intval($in->rank) . "', post = '" . intval($in->post) . "', type = '" . intval($in->chartype) . "' WHERE id = " . $in->id;
		if ($con2->query($sql) === TRUE) {
			ack();
		} else {
			echo "Error: " . $sql . "<br>" . $con2->error;
		}
		break;
	
	case "shipadd":
		$sql = "INSERT INTO forum_ship (`id`, `name`, `number`, `faction`, `image`, `link`, `crew`, `active`) VALUES ('" . $in->id . "', '" . $in->name . "', '" . $in->number . "', '1', '" . $in->image . "', '" . $in->link . "', '" . $in->crew . "', '" . $in->active . "')";
		$q1 = $con2->query($sql);
		$sql = "INSERT INTO forum_ship_post (`ship_id`, `post_id`) VALUES ('" . $in->id . "', '1'),('" . $in->id . "', '2'),('" . $in->id . "', '3'),('" . $in->id . "', '4'),('" . $in->id . "', '5'),('" . $in->id . "', '6'),('" . $in->id . "', '7'),('" . $in->id . "', '8'),('" . $in->id . "', '9'),('" . $in->id . "', '10')";
		$q2 = $con2->query($sql);
		if (($q1 === TRUE) && ($q2 === TRUE)) {
			ack();
		} else {
			echo "Error: " . $sql . "<br>" . $con2->error;
		}
		break;
		
	case "shipedit":
		$sql = "UPDATE forum_ship SET name = '" . $in->name . "', number = '" . $in->number . "', image = '" . $in->image . "', link = '" . $in->link . "' WHERE id = " . $in->id;
		if ($con2->query($sql) === TRUE) {
			ack();
		} else {
			echo "Error: " . $sql . "<br>" . $con2->error;
		}
		break;
	
	case "offdel":
		$sql = "DELETE FROM forum_char WHERE id = " . $in->id;
		$q1 = $con2->query($sql);
		$sql = "UPDATE forum_ship_post SET char_id = NULL WHERE char_id = " . $in->id;
		$q2 = $con2->query($sql);
		if (($q1 === TRUE) && ($q2 === TRUE)) {
			ack();
		} else {
			echo "Error: " . $sql . "<br>" . $con2->error;
		}
		break;
	
	case "shipdel":
		$sql = "DELETE FROM forum_ship WHERE id = " . $in->id;
		$q1 = $con2->query($sql);
		$sql = "DELETE FROM forum_ship_post WHERE ship_id = " . $in->id;
		$q2 = $con2->query($sql);
		if (($q1 === TRUE) && ($q2 === TRUE)) {
			ack();
		} else {
			echo "Error: " . $sql . "<br>" . $con2->error;
		}
		break;
				
	case "offmove":
		$q1 = TRUE;
		$q3 = TRUE;
		if($in->hadship == 1)
		{
			$sql = "UPDATE forum_ship_post SET char_id = NULL WHERE char_id = " . $in->id;
			$q1 = $con2->query($sql);
		}
		if(strcmp($in->dest, "holiday") == 0)
		{
			$sql = "UPDATE forum_char SET holiday = 1 WHERE id = " . $in->id;
			$q2 = $con2->query($sql);
		}
		else
		{
			$sql = "UPDATE forum_char SET holiday = 0 WHERE id = " . $in->id;
			$q2 = $con2->query($sql);
			$sql = "UPDATE forum_ship_post SET char_id = " . $in->id . " WHERE ship_id = " . $in->dest . " AND post_id = " . $in->post;
			$q3 = $con2->query($sql);
		}
		if (($q1 === TRUE) && ($q2 === TRUE) && ($q3 === TRUE)) {
			ack();
		} else {
			echo "Error: " . $sql . "<br>" . $con2->error;
		}
		break;
		
	case "list":
		//fetch crew data
		$crewdata = array();
		if ($result = mysqli_query($con2, "SELECT * FROM forum_ship_post")) {
			while($row = $result->fetch_array(MYSQLI_ASSOC)) {
				if($row["char_id"] != NULL) {
					$crewdata[intval($row["ship_id"])][intval($row["post_id"])] = intval($row["char_id"]);
				}
				else {
					$crewdata[intval($row["ship_id"])][intval($row["post_id"])] = "null";
				}
			}
			mysqli_free_result($result);
		}
	
		if ($result = mysqli_query($con2, "SELECT * FROM forum_char")) {
			while($row = $result->fetch_array(MYSQLI_ASSOC)) {
				$custom = array();
				$custom["id"] = intval($row["id"]);
				$custom["link_id"] = intval($row["link_id"]);
				$custom["forum_id"] = intval($row["forum_id"]);
				$custom["player_id"] = intval($row["player_id"]);
				$custom["rank"] = intval($row["rank"]);
				$custom["post"] = intval($row["post"]);
				$custom["chartype"] = intval($row["type"]);
				$custom["holiday"] = intval($row["holiday"]);
				$custom["name"] = utf8_encode($row["name"]);
				$data["officers"][intval($row["id"])] = $custom;
			}
			// Free result set
			mysqli_free_result($result);
		}
		if ($result = mysqli_query($con2, "SELECT * FROM forum_ship")) {
			while($row = $result->fetch_array(MYSQLI_ASSOC)) {
				$custom = array();
				$custom["id"] = intval($row["id"]);
				$custom["faction"] = intval($row["faction"]);
				$custom["active"] = intval($row["active"]);
				$custom["name"] = utf8_encode($row["name"]);
				$custom["number"] = utf8_encode($row["number"]);
				$custom["image"] = utf8_encode($row["image"]);
				$custom["link"] = utf8_encode($row["link"]);
				//organize crewdata into json-arrays
				/*$crewstr = "";
				foreach($crewdata[intval($row["id"])] as $x => $value)
				{
					$crewstr = $crewstr . '"' . $x . '"' . ':' . $value . ',';
				}
				$custom["crew"] = utf8_encode(rtrim($crewstr, ","));*/
				$crew = array();
				foreach($crewdata[intval($row["id"])] as $x => $value)
				{
					array_push($crew, $value);
				}
				$custom["crew"] = $crew;//json_encode($crew, JSON_FORCE_OBJECT);
				$data["ships"][intval($row["id"])] = $custom;
			}
			// Free result set
			mysqli_free_result($result);
		}
		$arr = array("type"=>"list", "data"=>$data);
		$str_json = stripcslashes(json_encode($arr, JSON_UNESCAPED_UNICODE));
		echo $str_json;
		break;
}

function ack()
{
	$arr = array("type"=>"succ");
	$str_json = json_encode($arr);
	echo $str_json;
}

mysqli_close($con2);
?>
