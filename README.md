# stinfinity

Curated selection of specialized pages by Kevin Roese for the German Star Trek roleplay forum [Star Trek - Infinity](https://www.startrek-online.com/)

To ease administration of the large roleplaying forum, a variety of features was developed in 2020.

Frontend: Plain HTML + JavaScript, CSS provided by forum
Backend: PHP, MySQL Database (data storage previously handled using a JSON file)

## Avatarliste

Due to forum rules as well as copyright purposes, administration had to keep track of the avatars of the different characters, for which pictures of actors were commonly used.
The list of avatars is visible to any forum member, while the administration page "avatarliste_verwaltung.html" is only visible to administrators.

## Personalverwaltung

For various personnel and administrative tasks, such as crew transfers, posting reminders and character awards, at first various text generators were created.
At a later point, some administrative functions have been rolled into "personalverwaltung.html" and expanded from a simple text generation system to an administration system with automated updating of publicly visible lists for showing the ships and their crews ("schiffsliste.html") and displaying free positions ("freiestellen.html")